import axios from 'axios';
import { url } from 'api/config';

export const ApiGetProjectList = () => {
  return axios.get(url  + '/index');
};
export const ApiDeleteProject = data => {
  return axios.post(url + '/index/delete', data);
};
export const ApiCreateProject = data => {
  return axios.post(url + '/create/project', data);
};

export const ApiSetLogo = data => {
  return axios.post(url + '/index/setlogo', data);
};
