import axios from 'axios';
import { url } from 'api/config';

export const ApiGetJwt = data => {
  return axios.post(url + '/auth/login', data);
};

export const ApiLogout = () => {
  return axios.post(url + '/auth/logout');
};

export const ApiCreateBucket = (data) => {
  return axios.post(url + '/auth/createBucket', data);
};