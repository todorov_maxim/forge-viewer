import React from 'react';
import { Route } from 'react-router-dom';
import Main from 'components/main';
import CreateProject from 'components/createProject';
import Viewer from 'components/viewer';

export const routes = [
  <Route key="3" path="/viewer" component={Viewer} />,
  <Route key="2" path="/create" component={CreateProject} />,
  <Route key="1" path="/" component={Main} />,
];
