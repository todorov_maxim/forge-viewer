import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { history } from 'store';
import { authentication } from './authentication';
import { projectList } from './projectList';
import { currentProject } from './projectList';
import { userLogo } from './userLogo';

export const rootReducer = combineReducers({
  router: connectRouter(history),
  authentication,
  projectList,
  currentProject,
  userLogo
});
