const initialState = '';

export const userLogo = (state = initialState, action) => {
  switch (action.type) {
    case 'SETLOGO':
      return action.logo;
    default:
      return state;
  }
};
