const initialState = {};

export const authentication = (state = initialState, action) => {
  switch (action.type) {
    case 'GETJWT':
      return action.auth
    default:
      return state;
  }
};
