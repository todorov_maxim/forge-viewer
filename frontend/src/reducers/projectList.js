const initialState = [];
const currentProjectState = null;

export const projectList = (state = initialState, action) => {
  switch (action.type) {
    case 'LIST':
      return action.list;
    case 'DELETE':
      return state.filter(item => item._id !== action.id);
    case 'NEW':
      return [...state, action.new];
    default:
      return state;
  }
};

export const currentProject = (state = currentProjectState, action) => {
  switch (action.type) {
    case 'SETPROJECT':
      return action.project;
    default:
      return state;
  }
};