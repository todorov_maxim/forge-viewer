import { ApiGetJwt, ApiLogout, ApiCreateBucket } from 'api/authentication';
import setAuthToken from '../setAuthToken';
import { history } from 'store';

export const GetJwt = data => dispatch => {

  ApiGetJwt(data)
    .then(res => {
      console.log(res)
      setAuthToken(res.data.token);
      localStorage.setItem('auth', res.data.token);
      localStorage.setItem('access_token', res.data.access_token);
      localStorage.setItem('expires_in', res.data.expires_in);
      localStorage.setItem('localStorageInitTime', +new Date());
      dispatch({ type: 'GETJWT', auth: {auth : res.data.token, access_token :  res.data.access_token, expires_in: res.data.expires_in}});
    })
    .catch(err => {
      dispatch({ type: 'GETJWT', auth: 'failed' });
    });
};

export const Out = () => dispatch => {
  ApiLogout()
    .then(res => {
      setAuthToken();
      localStorage.clear();
      dispatch({ type: 'GETJWT', auth: "" });
      history.push('/');
    })
    .catch(err => {
    });
};

export const CreateBucket = (data) => dispatch => {
  ApiCreateBucket(data)
    .then(res => {

    })
    .catch(err => {
    });
};