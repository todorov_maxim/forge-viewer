import {
  ApiGetProjectList,
  ApiDeleteProject,
  ApiCreateProject,
  ApiSetLogo
} from 'api/projectList';
import { history } from 'store';
import { url } from 'api/config';
import loadImg from 'assets/load-img.gif';
import setAuthToken from '../setAuthToken';


export const GetProjectList = () => dispatch => {
  ApiGetProjectList()
    .then(res => {
      dispatch({ type: 'LIST', list: res.data.projects });
      dispatch({ type: 'SETLOGO', logo: url + res.data.logo });
    })
    .catch(err => {
        setAuthToken();
        localStorage.removeItem('auth');
        dispatch({ type: 'GETJWT', auth: "" });
        history.push('/');
    });
};
export const DeleteProject = data => dispatch => {
  ApiDeleteProject(data)
    .then(res => {
      dispatch({ type: 'DELETE', id: data.id });
    })
    .catch(err => {});
};
export const NewProject = data => dispatch => {
  ApiCreateProject(data)
    .then(res => {
      dispatch({ type: 'NEW', new: res.data });
      dispatch({ type: 'SETPROJECT', project: res.data });
      history.push('/viewer');
    })
    .catch(err => {
      
    });
};
export const OpenProject = data => dispatch => {
  dispatch({ type: 'SETPROJECT', project: data });
  history.push('/viewer');
};

export const SetUserLogo = data => dispatch => {
  dispatch({ type: 'SETLOGO', logo: loadImg });

  ApiSetLogo(data)
    .then(res => {
      dispatch({ type: 'SETLOGO', logo: url + res.data });
    })
    .catch(err => {
      
    });
};

