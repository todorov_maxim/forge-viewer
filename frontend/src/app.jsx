import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { store, history } from 'store';

import setAuthToken from 'setAuthToken'
import Authentication from 'components/authentication';


if (localStorage.auth) {
    let limit = 1 * 3600 * 1000;
    if(+new Date() - localStorage.getItem('localStorageInitTime') > limit){
        localStorage.clear();
    }else{
        setAuthToken(localStorage.auth);
        store.dispatch({ type: 'GETJWT', auth: {auth : localStorage.auth, expires_in : localStorage.expires_in , access_token : localStorage.access_token} });
    }
}

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Authentication />
    </ConnectedRouter>
  </Provider>
);

export default App;
