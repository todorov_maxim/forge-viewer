import React from 'react';


const SetLogo = ({ SetUserLogo }) => {

    const set = (event) =>{

    let file = event.target.files[0];
    let formData = new FormData();
    formData.append('filedata', file);
    if(file.type === "image/jpeg" || file.type === "image/png"){
      SetUserLogo(formData);
    }else{
        alert( "You can upload PNG, JPEG" );
    }

    }
 
  return (
    <div  className="nav-btn set-logo">
        <input
              type="file"
              className="custom-file-input"
              name="file"
              onChange={set}
            />
    </div>
  );
};

export default SetLogo;
