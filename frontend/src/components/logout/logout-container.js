import Logout from './logout';
import { connect } from 'react-redux';
import { Out } from 'actions/authentication';

export default connect(
  state => ({

  }),
  {
    Out
  },
)(Logout);
