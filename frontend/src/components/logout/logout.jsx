import React from 'react';


const Logout = ({ Out }) => {

    const clickOut = () => {
        Out();
    }

 
  return (
    <div  className="out" onClick={clickOut}>
        Exit
    </div>
  );
};

export default Logout;
