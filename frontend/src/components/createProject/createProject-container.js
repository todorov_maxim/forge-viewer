import CreateProject from './createProject';
import { connect } from 'react-redux';
import { NewProject } from 'actions/projectList';

export default connect(
  state => ({
    projectList: state.projectList,
    auth: state.authentication
  }),
  {
    NewProject,
  },
)(CreateProject);
