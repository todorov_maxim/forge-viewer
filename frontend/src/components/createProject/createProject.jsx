import React, { useState } from 'react';
import authLogo from 'assets/auth-logo.png';
import loadImg from 'assets/load.gif';
import classnames from 'classnames';

const CreateProject = ({ projectList, auth, NewProject }) => {
  const [name, setName] = useState('');
  const [location, setLocation] = useState('');
  const [invite, setInvite] = useState('');
  const [file, setFile] = useState('');
  const [load, setLoad] = useState(false);

  const createSubmit = event => {
    event.preventDefault();
    let formData = new FormData();
    formData.append('filedata', file);
    formData.append('name', name);
    formData.append('location', location);
    formData.append('teamName', invite);
    formData.append('token', auth.access_token);
    setLoad(true)
        NewProject(formData);
    /*if(file.type === "image/jpeg" || file.type === "image/png" || file.type === "application/pdf"){
        
    }else{
        alert( "You can upload PDF, PNG, JPEG" );
    }*/
  };

  return (
    <div className="auth-page create-roject">
      <div className={classnames('loading', {
            'load': load })}>
              <img src={loadImg} alt="load" />
            </div>
      <form onSubmit={createSubmit} className="auth-wrapper">
        <div className="auth-logo">
          <img src={authLogo} alt="logo" />
        </div>
        <div className="input-container">
          <div className="input-wrapper">
            <div className="input-text">
              To start a project you need to fill in the folowing:
            </div>
          </div>
          <div className="input-wrapper">
            <label>Project Name</label>
            <input
              type="text"
              name="ProjectName"
              value={name}
              onChange={event => setName(event.target.value)}
              required 
            />
          </div>
          <div className="input-wrapper">
            <label>Location</label>
            <input
              type="text"
              name="Location"
              value={location}
              onChange={event => setLocation(event.target.value)}
              required 
            />
          </div>
          <div className="input-wrapper">
            <label>Invite Team</label>
            <input
              type="text"
              name="Invite"
              value={invite}
              onChange={event => setInvite(event.target.value)}
              required 
            />
          </div>
          <div className="input-wrapper">
            <input
              type="file"
              className={classnames('custom-file-input', {
                'file-done': file !== '',
              })}
              name="file"
              onChange={event => setFile(event.target.files[0])}
              required 
            />
          </div>
          <div className="input-wrapper">
            <div className="input-text">You can upload PDF, PNG, JPEG</div>
          </div>
        </div>
        <div className="btn-wrapper">
          <button className="btn">
            Start <br /> New Project
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreateProject;
