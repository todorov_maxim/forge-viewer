import React, { useEffect, useState } from 'react';
import { url } from 'api/config';
import classnames from 'classnames';
import { ReactComponent as IconMore } from 'assets/more.svg';
import { history } from 'store';
import SetLogo from 'components/setLogo';


const Main = ({ projectList, logo, auth, GetProjectList, DeleteProject, OpenProject, CreateBucket }) => {
  const [page, setPage] = useState(1);

  useEffect(() => {
    GetProjectList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ GetProjectList ]);

  const nextPage = () => {
    setPage(page + 1);
  };
  const deleteItem = event => {
    event.preventDefault();
    DeleteProject({ id: event.target.id });
  };

  const openProject = (e, item) => {
    if(!e.target.id){
      OpenProject(item)
    }
  };


  return (
    <div className="main">
      <div className="main-logo">
        <img src={logo} alt="logo" />
      </div>
      <nav className="navigation">
        <div
          className="nav-btn"
          onClick={() => {
            CreateBucket({token : auth.access_token})
            history.push('/create');
          }}
        >
          Start new project
        </div>
        <SetLogo />
      </nav>
      <div
        className={classnames('project-list', {
          'project-list-hide': projectList.length === 0,
        })}
      >
        {projectList.slice(0, page * 9).map(item => (
          <div key={item._id} className="project-item">
            <div className="img-wrapper" onClick={(e) => openProject(e, item)}>
              <img src={url + '/' + item.background} alt="plan" />
              <button id={item._id} onClick={deleteItem} className="delete-btn">
                Delete
              </button>
            </div>
            <div className="attributes">
              <div className="attr">{item.name}</div>
              <div className="attr">{item.location}</div>
              <div className="attr">{item.teamName}</div>
              <div className="attr">{item.history}</div>
            </div>
          </div>
        ))}
        <div
          onClick={nextPage}
          className={classnames('next-page', {
            'next-page-hide': projectList.length <= page * 9,
          })}
        >
          <IconMore />
        </div>
      </div>
    </div>
  );
};

export default Main;
