import Main from './main';
import { connect } from 'react-redux';
import { GetProjectList, DeleteProject, OpenProject } from 'actions/projectList';
import { CreateBucket } from 'actions/authentication';

export default connect(
  state => ({
    projectList: state.projectList,
    logo: state.userLogo,
    auth: state.authentication
  }),
  {
    GetProjectList,
    DeleteProject,
    OpenProject,
    CreateBucket
  },
)(Main);
