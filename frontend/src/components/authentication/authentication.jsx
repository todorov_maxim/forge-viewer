import React, { useState } from 'react';
import { Switch } from 'react-router-dom';
import { routes } from 'routes';
import classnames from 'classnames';
import authLogo from 'assets/auth-logo.png';
import Logout from 'components/logout';

const Authentication = ({ auth, GetJwt }) => {
  const [mail, setMail] = useState('');
  const [password, setPassword] = useState('');

  const loginSubmit = event => {
    event.preventDefault();
    GetJwt({ email : mail, password : password });
  };

  return (
    <div>
      {auth && auth !== 'failed' ? (
        <div>
          <Logout />
          <Switch>{routes}</Switch>
        </div>
      ) : (
        <div className="auth-page">
          <form
            onSubmit={loginSubmit}
            className={classnames('auth-wrapper', {
              'auth-failed': auth === 'failed',
            })}
          >
            <div className="auth-logo">
              <img src={authLogo} alt="logo" />
            </div>
            <div className="input-container">
              <div className="input-wrapper">
                <label>E-mail</label>
                <input
                  type="text"
                  name="mail"
                  value={mail}
                  onChange={event => setMail(event.target.value)}
                  required 
                />
              </div>
              <div className="input-wrapper">
                <label>Password</label>
                <input
                  type="password"
                  name="password"
                  value={password}
                  onChange={event => setPassword(event.target.value)}
                  required 
                />
              </div>
            </div>
            <div className="btn-wrapper">
              <button className="btn">Login in Plan Construction</button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
};

export default Authentication;
