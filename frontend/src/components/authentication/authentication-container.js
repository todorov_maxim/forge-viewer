import Authentication from './authentication';
import { connect } from 'react-redux';
import { GetJwt } from 'actions/authentication';

export default connect(
  state => ({
    auth: state.authentication.auth,
  }),
  {
    GetJwt,
  },
)(Authentication);
