import React from 'react';
import { ReactComponent as IconSearch } from 'assets/search.svg';
import crane from 'assets/crane.jpg';
import scissor_lift from 'assets/scissor_lift.png';
import rotating_crane from 'assets/rotating_crane.png';
import crane_350 from 'assets/crane_350.png';

const Library = ({setLoadAnim, loadModels,  setLoadModels }) => {



  return (
    <div  className="library">
        <div className="search-wrapper">
            <IconSearch className="search-icon" /> 
            <input type="text" name="search" className="input-search" placeholder="SEARCH.................................................." />
        </div>
        <div className="library-items-wrapper">

        
        <div className="library-item"  onClick={()=>{
            setLoadAnim(true);
            setLoadModels([{ urn: "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6aDBoYjZxeXI0aHZwb2RrY2VlbWo4Y2F3a2FuZWN2Z2xfZm9yZ2VfYnVja2V0L1Rvd2VyX0NyYW5lXzQwOTQucnZ0", xform: {x:0,y:0,z:0}}, ...loadModels])
          
        }}>
            <div className="item-img" style={{backgroundImage: "url("+crane+")"}}>
            </div>
            <div className="item-name">
                Tower Crane
            </div>
        </div>

        <div className="library-item"  onClick={()=>{
            setLoadAnim(true);
            setLoadModels([{ urn: "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6aDBoYjZxeXI0aHZwb2RrY2VlbWo4Y2F3a2FuZWN2Z2xfZm9yZ2VfYnVja2V0L1Rvd2VyX0NyYW5lXzQwOTQuaWZj", xform: {x:0,y:0,z:0}}, ...loadModels])
        }}>
            <div className="item-img" style={{backgroundImage: "url("+scissor_lift+")"}}>
            </div>
            <div className="item-name">
                Scissor Lift
            </div>
        </div>

        <div className="library-item"  onClick={()=>{
            setLoadAnim(true);
            setLoadModels([{ urn: "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6aDBoYjZxeXI0aHZwb2RrY2VlbWo4Y2F3a2FuZWN2Z2xfZm9yZ2VfYnVja2V0L1JvdGF0aW5nQ3JhbmUuZmJ4", xform: {x:0,y:0,z:0}}, ...loadModels])
        }}>
            <div className="item-img" style={{backgroundImage: "url("+rotating_crane+")"}}>
            </div>
            <div className="item-name">
                Rotating Crane
            </div>
        </div>

        <div className="library-item"  onClick={()=>{
            setLoadAnim(true);
            setLoadModels([{ urn: "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6aDBoYjZxeXI0aHZwb2RrY2VlbWo4Y2F3a2FuZWN2Z2xfZm9yZ2VfYnVja2V0LzM1MF9Ub25fQ3JhbmUuZmJ4", xform: {x:0,y:0,z:0}}, ...loadModels])
        }}>
            <div className="item-img" style={{backgroundImage: "url("+crane_350+")"}}>
            </div>
            <div className="item-name">
             Crane 350
            </div>
        </div>

        </div>
    </div>
  );
};

export default Library;
