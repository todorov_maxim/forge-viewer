import React from 'react';
import authLogo from 'assets/auth-logo.png';
import signage from 'assets/toolbar/signage.png';

const Toolbar = () => {

  return (
    <div  className="toolbar">
        <div className="toolbar-line">
            <div className="toggle">
                <label className="switch">
                    <input type="checkbox" />
                    <span className="slider round"></span>
                </label>
            </div>
            <div className="toolbar-icon"><img src={signage} alt="signage" /></div>
            <div className="toolbar-text">Signage</div>
        </div>


        <div className="toolbar-logo">
            <img src={authLogo} alt="logo" />
        </div>
    </div>
  );
};

export default Toolbar;
