import React from 'react';
import { ReactComponent as IconSave } from 'assets/icon/save.svg';
import { ReactComponent as IconExport } from 'assets/icon/export.svg';
import { ReactComponent as IconImport } from 'assets/icon/import.svg';
import { ReactComponent as IconScale } from 'assets/icon/scale.svg';
import { ReactComponent as IconUndo} from 'assets/icon/undo.svg';
import { ReactComponent as IconRedo } from 'assets/icon/redo.svg';
import { ReactComponent as IconText } from 'assets/icon/text.svg';
import { ReactComponent as IconDimension } from 'assets/icon/dimension.svg';
import { ReactComponent as IconLine } from 'assets/icon/line.svg';
import { ReactComponent as IconFill } from 'assets/icon/fill.svg';
import { ReactComponent as IconDelete } from 'assets/icon/delete.svg';
import { ReactComponent as IconViews } from 'assets/icon/views.svg';
import { ReactComponent as IconComment } from 'assets/icon/comment.svg';
import { ReactComponent as IconSettings } from 'assets/icon/settings.svg';

const Header = () => {

  return (

        <div className="header">
            <div className="group-icons">
                <div className="icon">
                    < IconSave className="icon-img" />
                    <div className="icon-text">
                        Save Project
                    </div>
                </div>

                <div className="icon">
                    < IconExport className="icon-img" />
                    <div className="icon-text">
                        Export Project
                    </div>
                </div>

                <div className="icon">
                    < IconImport className="icon-img" />
                    <div className="icon-text">
                        Import Project
                    </div>
                </div>

                <div className="icon red">
                    < IconScale className="icon-img" />
                    <div className="icon-text">
                        Scale Background
                    </div>
                </div>
            </div>

            <div className="group-icons">
                <div className="icon">
                    < IconUndo className="icon-img" />
                    <div className="icon-text">
                        Undo
                    </div>
                </div>

                <div className="icon">
                    < IconRedo className="icon-img" />
                    <div className="icon-text">
                        Redo
                    </div>
                </div>
            </div>

            <div className="group-icons">
                <div className="icon">
                    < IconText className="icon-img" />
                    <div className="icon-text">
                        Text
                    </div>
                </div>

                <div className="icon">
                    < IconDimension className="icon-img" />
                    <div className="icon-text">
                        Dimension
                    </div>
                </div>

                <div className="icon">
                    < IconLine className="icon-img" />
                    <div className="icon-text">
                        Line
                    </div>
                </div>

                <div className="icon">
                    < IconFill className="icon-img" />
                    <div className="icon-text">
                        Fill
                    </div>
                </div>

                <div className="icon">
                    < IconDelete className="icon-img" />
                    <div className="icon-text">
                        Delete
                    </div>
                </div>
            </div>

            <div className="group-icons">
                <div className="icon">
                    < IconViews className="icon-img" />
                    <div className="icon-text">
                        Views
                    </div>
                </div>

                <div className="icon">
                    < IconComment className="icon-img" />
                    <div className="icon-text">
                        Comment
                    </div>
                </div>
            </div>

            <div className="group-icons">
                <div className="icon">
                    < IconSettings className="icon-img" />
                    <div className="icon-text">
                        Settings
                    </div>
                </div>
            </div>


        </div>

  );
};

export default Header;
