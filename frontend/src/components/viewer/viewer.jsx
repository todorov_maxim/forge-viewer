import React, { useRef,  useEffect, useState } from 'react';
import classnames from 'classnames';
import { history } from 'store';
import Header from './header'
import Toolbar from './toolbar'
import Library from './library'
import loadImg from 'assets/load.gif';

//import "extensions/Viewing.Extension.Transform" 

const Autodesk = window.Autodesk;
const THREE = window.THREE
let viewer;
let index = 0;
let count = 0;
const Viewer = ({ project, aurh }) => {
  const forgeRef = useRef(null);
  const [loadAnim, setLoadAnim] = useState(false);
  const [loadModels, setLoadModels] = useState(project ? [{ urn: project.urn, xform: {x:0,y:0,z:0}}] : "not loaded project")

  const options = {
    env: 'AutodeskProduction',
    api: 'derivativeV2',
    getAccessToken: function(onSuccess) {
                    onSuccess(aurh.access_token, aurh.expires_in);
                }
  };
  const load2dModel = () => {
        console.log("function a")
        Autodesk.Viewing.Document.load(`urn:${loadModels[index].urn}`, (doc) => {
            var viewables = doc.getRoot().getDefaultGeometry();
            console.log("1111111", viewables.is2D())
            viewer.loadDocumentNode(doc, viewables,{
                placementTransform: (new THREE.Matrix4()).setPosition(loadModels[index].xform),
                keepCurrentModels: true,
                globalOffset: {x:0,y:0,z:0}
            })
            .then(res=> { 
                if(count === 0) {
                    viewer.loadExtension('Viewing.Extension.Transform').then(
                        externalExtension => {
                            externalExtension.translateTool.fullTransform = true
                            externalExtension.rotateTool.fullTransform = true
                            console.log(externalExtension)
                        }
                    )
                }
                count++;
                index++;
                
                if (index !== loadModels.length){
                    load2dModel();
                }else{
                    setLoadAnim(false);
                }
                
                console.log("finish", res)
                
            });
               
        });

  }

  const autodeskViewing = () => {
    
    Autodesk.Viewing.Initializer(options, function onInitialized(){  
    if(!viewer || viewer.impl.is2d){
      let htmlElement = forgeRef.current
     
      if (htmlElement) {

          viewer = new Autodesk.Viewing.Private.GuiViewer3D(htmlElement, {  extensions: ['_Viewing.Extension.ControlSelector'] });
          viewer.start();
        
          index = 0;
          count = 0;
        
          load2dModel();     
        //  Autodesk.Viewing.Document.load("urn:" + project.urn, onDocumentLoadSuccess, onDocumentLoadFailure);

      }
    }else{
        Autodesk.Viewing.Document.load("urn:" + loadModels[0].urn, onDocumentLoadSuccess, onDocumentLoadFailure);
    }
    });
  }

  const onDocumentLoadSuccess = (doc) => {

    let viewable = doc.getRoot().getDefaultGeometry();
    if (viewable) {
        viewer.loadDocumentNode(doc, viewable, {
            placementTransform: (new THREE.Matrix4()).setPosition({x:0,y:0,z:0}),
            keepCurrentModels: true,
            globalOffset: {x:0,y:0,z:0}
        }).then(function(result) {
            console.log('Viewable Loaded!', result);
            window.viewer = viewer;
            setLoadAnim(false);

        }).catch(function(err) {
            console.log('errrrrrrrrrrrrrrrrrr', err)
          setTimeout(() => {
            autodeskViewing()
          }, 1000);
        }
        )
    }
  }

    const  onDocumentLoadFailure = (viewerErrorCode) => {
    setTimeout(() => {
      autodeskViewing()
    }, 5000);
      
  }

  useEffect(() => {
    if(project){
     // setLoadModels([{ urn: project.urn, xform: {x:0,y:0,z:0}}])
      console.log(loadModels) 
    autodeskViewing()

      

    }else{
      history.push('/');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadModels]);

  return (
    <div  className="viewer-wrapper">
        < Header />
        < Toolbar />
        <div className="viewer-main" >
            <div className="properties" >
              <div className="properties-item">
                Scale by:
              </div>
              <div className="properties-item green">
                Percentage <div className="value">100%</div>
              </div>

              <div className="properties-item red">
                Two points
              </div>

              <div className="properties-item red">
                Distance: <div className="value" >1000.000</div>
              </div>

              <div className="properties-item red">
                Units: <div className="value" >mm</div>
              </div>

              <div className="toggle">
                  <label className="switch">
                      <input type="checkbox" />
                      <span className="slider round"></span>
                  </label>
              </div>
            </div>

            <div className="three-container" ref={forgeRef} style={{backgroundImage: "url("+loadImg+")"}}>
                <div className={classnames('load', {'load-anim': loadAnim })}></div>
              { //<img src={project && url + '/' + project.background} alt="project background" />
                }
            </div>

            <div className="footer" onClick={()=>{
            //setLoadAnim(true);
            //setLoadModels([{ urn: "dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6aDBoYjZxeXI0aHZwb2RrY2VlbWo4Y2F3a2FuZWN2Z2xfZm9yZ2VfYnVja2V0L1Rvd2VyX0NyYW5lXzQwOTQuZmJ4", xform: {x:0,y:0,z:0}}, ...loadModels])

           // Autodesk.Viewing.Document.load("urn:dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6aDBoYjZxeXI0aHZwb2RrY2VlbWo4Y2F3a2FuZWN2Z2xfZm9yZ2VfYnVja2V0L0EyLTAxLmR3Zw==", onDocumentLoadSuccess, onDocumentLoadFailure);
        }}>

            </div>

        </div>
        <Library setLoadAnim={setLoadAnim} loadModels={loadModels} setLoadModels={setLoadModels} />
    </div>
  );
};

export default Viewer;
