const mongoose = require('mongoose')
const passportLocalMongoose = require('passport-local-mongoose');

const userSchema = mongoose.Schema({
        _id: {
                type: mongoose.Schema.Types.ObjectId,
                auto: true
                },
        email: {
                type: String
                },
        password: String,
        logo: {
                type: String,
                default: '/public/images/logo.png'
        },

})

userSchema.plugin(passportLocalMongoose);
let User = mongoose.model('User', userSchema)

module.exports = User
userSchema.plugin(passportLocalMongoose);
