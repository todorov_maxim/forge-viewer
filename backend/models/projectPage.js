const mongoose = require('mongoose')

const projectPageSchema = mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true
        },
    name: {
                type: String,
                required: true,
                minlength:1
            },
    location: {
                type: String,
                required: true,
                minlength:1
            },
    teamName: {
            type: String,
            required: true
            },
    background: {
                    type: String,
                    required: true
                },
    urn: {
            type: String,

                },
    author: {
            type: Array,
            required: true
        },
    history: { 
            type: Date,
            default: Date.now
        }

})

const ProjectPage = mongoose.model('ProjectPage', projectPageSchema)

module.exports = ProjectPage
