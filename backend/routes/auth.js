const express = require('express');
const router = express.Router();
var AuthController = require('../controller/auth.controller');

// login user
router.post('/login', function(req, res, next) {
    AuthController.login(req, res);
});

// registration user
router.post('/registration', (req, res) => {
    AuthController.register(req, res);
});

router.post('/logout', (req, res, next) => {
    AuthController.logout(req, res);
});
router.post('/createBucket', (req, res, next) => {
    AuthController.createBucket(req, res);
});


module.exports = router;