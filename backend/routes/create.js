const express = require('express');
const router = express.Router();
const ProjectPage = require('../models/projectPage');
const User = require('../models/user');

var AuthController = require('../controller/auth.controller');

var multer = require('multer');         
var upload = multer({ dest: 'uploads/background' });

/* create new project. */
router.post('/project', upload.single('filedata'), function(req, res, next) {
    AuthController.createProject(req, res);
    
});

module.exports = router;
