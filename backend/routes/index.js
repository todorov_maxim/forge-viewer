const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const ProjectPage = require('../models/projectPage');
const User = require('../models/user');

var multer = require('multer');         
var upload = multer({ dest: 'uploads/logo' });

router.get('/', function(req, res, next) {
  
  ProjectPage.find({ author: req.user.username }, function (err, docs) {
    return res.json({logo: req.user.logo, projects: docs });
  }).sort({_id: -1});

});


router.post('/setlogo', upload.single('filedata'), function(req, res, next) {
  let newLogo = "/" + req.file.path

  User.findOneAndUpdate({username : req.user.username}, {logo : newLogo}, {new: true}, (err, doc) => {
    if (err) {
      console.log('err', err)
    }
    res.send(doc.logo)
  })

});


router.post('/delete', function(req, res, next) {
 /* ProjectPage.findOneAndDelete({"_id": req.body.id}, function (err, doc) {
    if (err) {
      res.sendStatus(500);
    }else{
      console.log(doc)
      res.sendStatus(200);
    }
  
  })*/
  ProjectPage.findOneAndUpdate({"_id": req.body.id}, { $pullAll: {author: [req.user.username] } } , function (err, doc) {
    if (err) {
        console.log('err', err)
    }
    console.log(doc)
    res.sendStatus(200);
})



});

module.exports = router;
