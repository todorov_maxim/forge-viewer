const User = require('../models/user');
const passport = require('passport');
const AuthController = {};
var jwt = require('jsonwebtoken');


var Axios = require('axios');  

const FORGE_CLIENT_ID = "H0hb6QyR4HvpoDkcEEmj8cAWKANeCVGl";
const FORGE_CLIENT_SECRET = "yvfcz9apGfywwGwG";

var access_token = '';
const scopes = 'data:read data:write data:create bucket:create bucket:read';
const querystring = require('querystring');





AuthController.register = async (req, res) => {
    try{
        User.register(new User({ username: req.body.email
            }), req.body.password, function(err, account) {
            if (err) {
                return res.status(500).send('An error occurred: ' + err);
            }

            passport.authenticate(
                'local', {
                    session: false
                })(req, res, () => {
                res.status(200).send('Successfully created new account');
            });
        });
    }
    catch(err){
        return res.status(500).send('An error occurred: ' + err);
    }
};

AuthController.login = async (req, res, next) => {
    try {
        if (!req.body.email || !req.body.password) {
            return res.status(400).json({
                message: 'Something is not right with your input'
            });
        }
        passport.authenticate('local', {session: false}, (err, user, info) => {
            if (err || !user) {
                return res.status(400).json({
                    message: 'Something is not right',
                    user   : user
                });
            }
            req.login(user, {session: false}, (err) => {
                if (err) {
                    res.send(err);
                }

                Axios({
                    method: 'POST',
                    url: 'https://developer.api.autodesk.com/authentication/v1/authenticate',
                    headers: {
                        'content-type': 'application/x-www-form-urlencoded',
                    },
                    data: querystring.stringify({
                        client_id: FORGE_CLIENT_ID,
                        client_secret: FORGE_CLIENT_SECRET,
                        grant_type: 'client_credentials',
                        scope: scopes
                    })
                })
                    .then(function (response) {
                        // Success
                        access_token = response.data.access_token;
                        let expires_in = response.data.expires_in
                        console.log(access_token, expires_in);
                        const token = jwt.sign({ id: user.id}, 'secretkey');
                        
                        res.json({ token: token,
                                    access_token: access_token,
                                    expires_in: expires_in
                         });
                    })
                    .catch(function (error) {
                        // Failed
                        console.log(error);
                        res.send('Failed to authenticate');
                    });

            });
        })(req, res);
    }
    catch(err){
        console.log(err);
    }
};

AuthController.logout = async (req, res) => {
    req.logout();
    res.status(200).send('Successfully logged out');
};
const bucketKey = FORGE_CLIENT_ID.toLowerCase() + '_forge_bucket'; 
const policyKey = 'transient'; 

AuthController.createBucket = async (req, res) => {

    Axios({
        method: 'POST',
        url: 'https://developer.api.autodesk.com/oss/v2/buckets',
        headers: {
            'content-type': 'application/json',
            Authorization: 'Bearer ' + req.body.token
        },
        data: JSON.stringify({
            'bucketKey': bucketKey,
            'policyKey': policyKey
        })
    })
        .then(function (response) {
            // Success

            detail(req.body.token)
            res.sendStatus(200)
            //res.redirect('/detail');
        })
        .catch(function (error) {
            if (error.response && error.response.status == 409) {
                console.log('Bucket already exists, skip creation.');
                //res.redirect('/detail');
            }
            // Failed
            console.log(error);
            detail(req.body.token)
            res.send('Failed to create a new bucket');
        });

}
function detail(token){

    Axios({
        method: 'GET',
        url: 'https://developer.api.autodesk.com/oss/v2/buckets/' + encodeURIComponent(bucketKey) + '/details',
        headers: {
            Authorization: 'Bearer ' + token
        }
    })
    .then(function (response) {
        // Success
        console.log(response);

    })
    .catch(function (error) {
        // Failed
        console.log(error);
    });

}

var Buffer = require('buffer').Buffer;
String.prototype.toBase64 = function () {
    // Buffer is part of Node.js to enable interaction with octet streams in TCP streams, 
    // file system operations, and other contexts.
    return new Buffer(this).toString('base64');
};

const ProjectPage = require('../models/projectPage');

AuthController.createProject = async (req, res) => {
    var fs = require('fs');

    console.log('wweewe', req.file.mimetype)
    if(req.file.mimetype === "image/jpeg" || req.file.mimetype === "image/png"){
        const im = require('imagemagick');
        im.convert([req.file.path , req.file.path + '.pdf'], function(err, stdout){
            if (err) throw err;

            fs.readFile(req.file.path +  '.pdf', function (err, filecontent) {
                Axios({
                    method: 'PUT',
                    url: 'https://developer.api.autodesk.com/oss/v2/buckets/' + encodeURIComponent(bucketKey) + '/objects/' + encodeURIComponent(req.file.filename + '.pdf'),
                    headers: {
                        Authorization: 'Bearer ' + req.body.token,
                        'Content-Disposition': req.file.filename + '.pdf',
                        'Content-Length': filecontent.length
                    },
                    data: filecontent
                })
                    .then(function (response) {
                        // Success
                        
                        var urn = response.data.objectId.toBase64();
                        modelderivative(req.body.token, urn)
    
                        const newProject = new ProjectPage({
                            name : req.body.name,
                            location : req.body.location,
                            teamName : req.body.teamName,
                            background: req.file.path,
                            urn : urn,
                            author : [req.user.username, req.body.teamName]
                        })
                        
                        newProject.save((err, newProject) => {
                            if (err) {
                                console.log('err', err)
                                res.sendStatus(500);
                            }
                            modelderivative(req.body.token, urn)
                            res.send(newProject)
                        })
    
                    })
                    .catch(function (error) {
                        // Failed
                        console.log(error);
                        res.send('Failed to create a new object in the bucket');
                    });
            });


        });

    }else{
        let bg = 'uploads/format/object.jpg'
        fs.readFile(req.file.path, function (err, filecontent) {
            Axios({
                method: 'PUT',
                url: 'https://developer.api.autodesk.com/oss/v2/buckets/' + encodeURIComponent(bucketKey) + '/objects/' + encodeURIComponent(req.file.originalname),
                headers: {
                    Authorization: 'Bearer ' + req.body.token,
                    'Content-Disposition': req.file.originalname,
                    'Content-Length': filecontent.length
                },
                data: filecontent
            })
                .then(function (response) {
                    // Success
                    
                    var urn = response.data.objectId.toBase64();
                    modelderivative(req.body.token, urn)

                    const newProject = new ProjectPage({
                        name : req.body.name,
                        location : req.body.location,
                        teamName : req.body.teamName,
                        background: bg,
                        urn : urn,
                        author : [req.user.username, req.body.teamName]
                    })
                    
                    newProject.save((err, newProject) => {
                        if (err) {
                            console.log('err', err)
                            res.sendStatus(500);
                        }
                        modelderivative(req.body.token, urn)
                        res.send(newProject)
                    })

                })
                .catch(function (error) {
                    // Failed
                    console.log(error);
                    res.send('Failed to create a new object in the bucket');
                });
        });
    }
}


function modelderivative(token, urn){
    console.log(urn)
    var format_type = 'svf';
    var format_views = ['2d', '3d'];
    Axios({
      method: 'POST',
      url: 'https://developer.api.autodesk.com/modelderivative/v2/designdata/job',
      headers: {
          'content-type': 'application/json',
          Authorization: 'Bearer ' + token
      },
      data: JSON.stringify({
          'input': {
              'urn': urn
          },
          'output': {
              'formats': [
                  {
                      'type': format_type,
                      'views': format_views
                  }
              ]
          }
      })
  })
      .then(function (response) {
          // Success
          console.log(urn)
          
      })
      .catch(function (error) {
          // Failed
          console.log(error);
        
      });

}


module.exports = AuthController;
